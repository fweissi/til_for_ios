//
//  HomeView.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/18/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import CoreData

struct HomeView: View {
    @Environment(\.editMode) var mode
    @Environment(\.managedObjectContext) var context
    @State private var selection = 0
    
    var body: some View {
        TabView(selection: $selection) {
            AcronymView()
                .tabItem {
                    AcronymView.tabItem
            }
            .tag(0)
            CategoryView()
                .tabItem {
                    VStack {
                        Image(systemName: "tag.fill")
                        Text("Categories")
                    }
            }
            .tag(1)
            UserView()
                .tabItem {
                    VStack {
                        Image(systemName: "person.fill")
                        Text("Users")
                    }
            }
            .tag(2)
        }
    }
}


#if DEBUG
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView().environment(\.managedObjectContext, appDelegate.viewContext)
    }
}
#endif
