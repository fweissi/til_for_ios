//
//  AcronymDetail.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/25/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import CoreData

struct AcronymDetail: View {
    @State var short: String = ""
    @State var long: String = ""
    @State var author: String = ""
    @Environment(\.editMode) var mode
    
    var body: some View {
        VStack(alignment: .center, spacing: 20) {
            HStack {
                Spacer()
                EditButton()
            }
            .padding(.horizontal, 20)
            Image("logo")
            Text("Today I Learned").font(.largeTitle)
            Spacer()
            if short.isEmpty {
                VStack(alignment: .leading, spacing: 20) {
                    Text("Select an acronym from the list of acronyms.").font(.title)
                    Text("If you do not see the list of acronyms, drag your finger from the left.").font(.caption).foregroundColor(.secondary)
                }
                .padding(.horizontal, 60)
            }
            else {
                if self.mode?.wrappedValue == .inactive {
                    DisplayAcronym(short: short, long: long, author: author)
                }
                else {
                    EditAcronym(short: $short, long: $long, author: author)
                }
            }
            Spacer()
        }
    }
}


struct EditAcronym: View {
    @Binding var short: String
    @Binding var long: String
    var author: String
    
    var body: some View {
        VStack(alignment: .leading, spacing: 20) {
            TextField("Short Value (Acronym)", text: self.$short)
            TextField("Long Value (What it means)", text: self.$long)
            HStack(alignment: .firstTextBaseline, spacing: 12) {
                Text("added by: ")
                    .foregroundColor(Color.secondary)
                    .font(.callout)
                Text(self.author).font(.title)
            }
            Button(action: {
                self.save()
            }) {
                Text("Save")
                    .foregroundColor(Color.white)
                    .padding(.horizontal, 40)
                    .padding(.vertical, 4)
                    .background(Color.blue)
                    .cornerRadius(12)
            }
        }
        .font(.largeTitle)
        .padding(.horizontal, 60)
        .textFieldStyle(RoundedBorderTextFieldStyle())
    }
    
    
    func save() {
        print("Save")
    }
}


struct DisplayAcronym: View {
    var short: String
    var long: String
    var author: String
    
    var body: some View {
        VStack(alignment: .leadingName, spacing: 20) {
            HStack(alignment: .firstTextBaseline, spacing: 12) {
                Text("Acronym: ")
                    .foregroundColor(Color.secondary)
                    .font(.subheadline)
                Text(self.short).font(.title).alignmentGuide(.leadingName) {d in d[.leading]}
            }
            HStack(alignment: .firstTextBaseline, spacing: 12) {
                Text("Meaning: ")
                    .foregroundColor(Color.secondary)
                    .font(.subheadline)
                Text(self.long).font(.title).alignmentGuide(.leadingName) {d in d[.leading]}
            }
            HStack(alignment: .firstTextBaseline, spacing: 12) {
                Text("added by: ")
                    .foregroundColor(Color.secondary)
                    .font(.callout)
                Text(self.author).font(.title).alignmentGuide(.leadingName) {d in d[.leading]}
                        Spacer()
            }
        }
        .padding(60)
    }
}


extension HorizontalAlignment {
    private enum LeadingName : AlignmentID {
        static func defaultValue(in d: ViewDimensions) -> CGFloat { d[.leading] }
    }
    static let leadingName = HorizontalAlignment(LeadingName.self)
}


struct AcronymDetail_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            AcronymDetail(short: "BRB", long: "Be Right Back", author: "Keith Weiss").environment(\.editMode, .constant(.inactive))
            AcronymDetail(short: "BRB", long: "Be Right Back", author: "Keith Weiss").environment(\.editMode, .constant(.active))
        }
    }
}
