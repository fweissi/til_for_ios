//
//  UserView.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/25/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import CoreData

struct UserView: View {
    @Environment(\.managedObjectContext) var context
    @FetchRequest(fetchRequest: User.fetchAll()) var users: FetchedResults<User>
    
    static var tabItem: some View {
        VStack {
            Image(systemName: "book.fill")
            Text("Acronyms")
        }
    }
    
    var body: some View {
        NavigationView {
            VStack {
                if users.count > 0 {
                    List(self.users) { (user) in
                        UserCell(user: user)
                    }
                }
                else {
                    Text("There are no users. You should add yourself as a user.")
                        .padding(.horizontal, 40)
                        .font(.title)
                }
            }
            .navigationBarTitle("Users")
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}


struct UserCell: View {
    var user: User
    
    var body: some View {
        VStack(alignment: .leading) {
            Text(user.username ?? "Unknown")
                .font(.headline)
            Text(user.name ?? "Unknown")
        }
    }
}


struct UserView_Previews: PreviewProvider {
    static var previews: some View {
        UserView()
    }
}
