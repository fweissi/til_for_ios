//
//  Acronym.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/18/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import Foundation
import CoreData

final public class Acronym: NSManagedObject, Identifiable, Codable {
    enum CodingKeys: String, CodingKey {
        case id, userID
        case short = "short"
        case long = "long"
    }

    @NSManaged public var id: Int64
    @NSManaged public var short: String?
    @NSManaged public var long: String?
    @NSManaged public var userID: String?
    @NSManaged public var user: User?
    
    required convenience public init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Acronym", in: managedObjectContext) else {
                fatalError("Failed to decode Acronym")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int64.self, forKey: .id)
        self.short = try container.decodeIfPresent(String.self, forKey: .short) ?? ""
        self.long = try container.decodeIfPresent(String.self, forKey: .long) ?? ""
        self.userID = try container.decode(String.self, forKey: .userID)
        
        guard let user = try? User.fetchUUID(self.userID!, in: managedObjectContext) else { throw NetworkError.uuidNotFound }
        
        self.user = user
    }
    
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(short, forKey: .short)
        try container.encode(long, forKey: .long)
        try container.encodeIfPresent(user?.id, forKey: .userID)
    }
    
    
    public static func fetchAll() -> NSFetchRequest<Acronym> {
        let fetchRequest = Acronym.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "short", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest as! NSFetchRequest<Acronym>
    }
    
    
    public static func updateUsers(in context: NSManagedObjectContext) throws {
        let fetchRequest = Acronym.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "user = nil")
        if let updateAcronyms = try context.fetch(fetchRequest) as? [Acronym] {
            try updateAcronyms.forEach { (acronym) in
                if let userID = acronym.userID {
                    acronym.user = try User.fetchUUID(userID, in: context)
                }
                try context.save()
            }
        }
    }
}


public extension CodingUserInfoKey {
    // Helper property to retrieve the context
    static let managedObjectContext = CodingUserInfoKey(rawValue: "managedObjectContext")
}
