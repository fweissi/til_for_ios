//
//  User.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/18/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import UIKit
import CoreData

enum NetworkError: Error, LocalizedError {
    case noDataReturned
    case deserializationFailed
    case uuidNotFound
    case failedToCacheData
    
    var errorDescription: String? {
        switch self {
        case .noDataReturned:
            return "No data"
        case .deserializationFailed:
            return "Bad deserialization"
        case .uuidNotFound:
            return "ID not found"
        case .failedToCacheData:
            return "Failed to cache results"
        }
    }
}

final public class User: NSManagedObject, Identifiable, Codable {
    enum CodingKeys: String, CodingKey {
        case id, name, username
    }
    
    @NSManaged public var id: String
    @NSManaged public var name: String?
    @NSManaged public var username: String?
    
    public class func fetchUUID(_ uuid: String, in context: NSManagedObjectContext) throws -> User {
        let fetchRequest = self.fetchRequest()
        fetchRequest.fetchLimit = 1
        fetchRequest.predicate = NSPredicate(format: "id == %@", uuid)
        
        guard let user = try? context.fetch(fetchRequest).first else { throw NetworkError.uuidNotFound }
        return user as! User
    }
    
    
    required convenience public init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "User", in: managedObjectContext) else {
            fatalError("Failed to decode User")
        }

        self.init(entity: entity, insertInto: managedObjectContext)

        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decodeIfPresent(String.self, forKey: .name)
        self.username = try container.decodeIfPresent(String.self, forKey: .username)
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(username, forKey: .username)
    }
    
    
    public static func fetchAll() -> NSFetchRequest<User> {
        let fetchRequest = User.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "username", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest as! NSFetchRequest<User>
    }
}
