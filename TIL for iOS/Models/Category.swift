//
//  Category.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/20/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import Foundation
import CoreData

final public class Category: NSManagedObject, Identifiable, Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case name = "name"
    }

    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    
    required convenience public init(from decoder: Decoder) throws {
        guard let codingUserInfoKeyManagedObjectContext = CodingUserInfoKey.managedObjectContext,
            let managedObjectContext = decoder.userInfo[codingUserInfoKeyManagedObjectContext] as? NSManagedObjectContext,
            let entity = NSEntityDescription.entity(forEntityName: "Category", in: managedObjectContext) else {
                fatalError("Failed to decode Category")
        }
        
        self.init(entity: entity, insertInto: managedObjectContext)
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(Int64.self, forKey: .id)
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
    }
    
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
    }
    
    
    public static func fetchAll() -> NSFetchRequest<Category> {
        let fetchRequest = Category.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        return fetchRequest as! NSFetchRequest<Category>
    }
}
