//
//  AppDelegate.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/18/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import UIKit
import CoreData
import Combine

let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    lazy var viewContext: NSManagedObjectContext = {
        return self.persistentContainer.viewContext
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        let getUserSubscriber: AnyCancellable = getAll("users").sink { (users) in
            guard !users.isEmpty else { return }
            self.persistentContainer.performBackgroundTask { (backgroundContext) in
                do {
                    try self.convertTo(entityNamed: "User", objects: users, in: backgroundContext)
                }
                catch {
                    print("Something bad happened. \(error)")
                }
            }
        }
        getUserSubscriber.cancel()
        
        let getCategorySubscriber: AnyCancellable = getAll("categories").sink { (categories) in
            guard !categories.isEmpty else { return }
            self.persistentContainer.performBackgroundTask { (backgroundContext) in
                do {
                    try self.convertTo(entityNamed: "Category", objects: categories, in: backgroundContext)
                }
                catch {
                    print("Something bad happened. \(error)")
                }
            }
        }
        getCategorySubscriber.cancel()
        
        return true
    }
    
    
    func convertTo(entityNamed: String, objects: [[String: Any]], in context: NSManagedObjectContext) throws {
        guard let entity = NSEntityDescription.entity(forEntityName: entityNamed, in: context) else { throw NetworkError.failedToCacheData }
        
        let insertRequest = NSBatchInsertRequest(entity: entity, objects: objects)
        let insertResult = try context.execute(insertRequest) as? NSBatchInsertResult
        guard let success = insertResult?.result as? Bool else { throw NetworkError.failedToCacheData }
        
        if success {
            try context.save()
        }
    }
    

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "TIL_for_iOS")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    
    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    
    func getAll(_ entityName: String) -> AnyPublisher<[[String: Any]], Never> {
        return URLSession.shared.dataTaskPublisher(for: URL(string: "https://todayilearnedapp.herokuapp.com/api/\(entityName)")!)
            .tryMap({ (dataResponse) in
                guard !dataResponse.data.isEmpty else { throw NetworkError.noDataReturned }
                guard let dictionaries = try? JSONSerialization.jsonObject(with: dataResponse.data, options: []) as? [[String: Any]] else { throw NetworkError.deserializationFailed }
                
                return dictionaries
            })
            .replaceError(with: [[String: Any]]())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}


//  MARK: NOTIFICATIONS

extension Notification.Name {
    static let DidUpdateCoreData = Notification.Name("DidUpdateCoreData")
}

