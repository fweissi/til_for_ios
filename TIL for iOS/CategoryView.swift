//
//  CategoryView.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/25/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import CoreData

struct CategoryView: View {
    @Environment(\.managedObjectContext) var context
    @FetchRequest(fetchRequest: Category.fetchAll()) var categories: FetchedResults<Category>
    
    var body: some View {
        NavigationView {
            VStack {
                if categories.count > 0 {
                    List(self.categories) { (category) in
                        CategoryCell(category: category)
                    }
                }
                else {
                    Text("There are no categories. You should add some.").padding(.horizontal, 40)
                        .font(.title)
                }
            }
            .navigationBarTitle("Categories")
        }
        .navigationViewStyle(StackNavigationViewStyle())
    }
}


struct CategoryCell: View {
    var category: Category
    
    var body: some View {
        Text(category.name ?? "Unknown")
    }
}


struct CategoryView_Previews: PreviewProvider {
    static var previews: some View {
        CategoryView()
    }
}
