//
//  AcronymView.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/25/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import CoreData

struct AcronymView: View {
    @Environment(\.managedObjectContext) var context
    
    static var tabItem: some View {
        VStack {
            Image(systemName: "book.fill")
            Text("Acronyms")
        }
    }
    
    
    var body: some View {
        NavigationView {
            AcronymMaster()
            AcronymDetail()
        }
    }
}


struct AcronymView_Previews: PreviewProvider {
    static var previews: some View {
        AcronymMaster().environment(\.managedObjectContext, appDelegate.viewContext)
    }
}
