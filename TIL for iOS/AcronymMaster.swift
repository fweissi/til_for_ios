//
//  AcronymMaster.swift
//  TIL for iOS
//
//  Created by Keith Weiss on 8/25/19.
//  Copyright © 2019 Keith Weiss. All rights reserved.
//

import SwiftUI
import CoreData

struct AcronymMaster: View {
    @Environment(\.managedObjectContext) var context
    @FetchRequest(fetchRequest: Acronym.fetchAll()) var acronyms: FetchedResults<Acronym>
    
    var body: some View {
        VStack {
            if acronyms.count > 0 {
                List(acronyms) { (acronym) in
                    NavigationLink(destination: AcronymDetail(short: acronym.short!, long: acronym.long!, author: acronym.user!.name!)) {
                        AcronymCell(acronym: acronym)
                    }
                }
            }
            else {
                Text("There are no acronyms. You should add some.")
                    .font(.title)
                    .padding(.horizontal, 40)
            }
        }
        .navigationBarTitle("Acronyms")
        .onReceive(appDelegate.getAll("acronyms")) { (acronyms) in
            guard !acronyms.isEmpty else { return }
            let context: NSManagedObjectContext = appDelegate.viewContext
            context.perform {
                do {
                    try appDelegate.convertTo(entityNamed: "Acronym", objects: acronyms, in: context)
                    try Acronym.updateUsers(in: context)
                }
                catch {
                    print("Something bad happened. \(error)")
                }
            }
        }
    }
}


struct AcronymCell: View {
    var acronym: Acronym
    
    var body: some View {
        VStack(alignment: .leading) {
            HStack {
                Text(acronym.short ?? "Unknown")
                    .font(.headline)
                Spacer()
                Text(acronym.user?.name ?? "Unknown")
                    .font(.subheadline)
                    .foregroundColor(.secondary)
            }
            Text(acronym.long ?? "Unknown")
        }
    }
}


struct AcronymMaster_Previews: PreviewProvider {
    static var previews: some View {
        AcronymMaster().environment(\.managedObjectContext, appDelegate.viewContext)
    }
}
